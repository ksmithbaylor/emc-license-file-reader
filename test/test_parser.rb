require 'minitest/spec'
require 'minitest/autorun'
require 'tempfile'

require './lib/ruby/EMCParser'
require './test/definitions'

describe EMCParser do
  
  it "can be created with no arguments" do
    EMCParser.new.must_be_instance_of EMCParser
  end

  it "can be created from a file" do
    file = Tempfile.new "test"
    EMCParser.new(file).must_be_instance_of EMCParser
  end

  it "has the right sections" do
    correct_sections = [:Module, :Copies, :Pages, :Valid, :EnterBy,
                        :Features, :Issued, :Disables, :Code]
    EMCParser.new.sections.must_equal correct_sections
  end

  it "must parse a trivial file correctly" do
    file = File.new "test/files/trivial.lic"
    fp = EMCParser.new(file)
    fp.parse
    file.close
    fp.modules.must_equal KTrivialArray
  end

  it "must parse an all-comments file correctly" do
    file = File.new "test/files/all_comments.lic"
    fp = EMCParser.new(file)
    fp.parse
    file.close
    fp.modules.must_equal KAllComments
  end

  it "must parse an actual license file correctly" do
    file = File.new "test/files/actual_license.lic"
    fp = EMCParser.new(file)
    fp.parse
    file.close
    fp.modules.must_equal KActualArray
  end

end