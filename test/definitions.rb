KTrivialArray = [%w(this is a test of the license file reader),
                 %w(it should always have exactly nine words per line),
                 %w(there should never be any new lines included here)]
KActualArray = [%W(ANNUAL 0 1000000 0 100117 #{""} 091217 #{""} 2748-13616/A),
                %W(SERVER #{""} ANNUAL 0 #{""} CSR 090821 #{""} 39833-97480),
                %W(GROUP4 2 0 0 100117 #{""} 091217 #{""} 7392-91840),
                %W(IAFAFIDX GROUP4 GROUP4 0 #{""} X 090821 #{""} 10785-47126),
                %W(IAIQA GROUP4 GROUP4 0 #{""} #{""} 090821 #{""} 12822-49426),
                %W(INDXPLUS GROUP4 GROUP4 0 #{""} X 090821 #{""} 10834-62549),
                %W(RSCANPLS GROUP4 GROUP4 0 #{""} DX 090821 #{""} 6870-84452),
                %W(DPCLSSFE GROUP4 GROUP4 0 #{""} BCDEGHIX 090821 #{""} 6148-63749),
                %W(DPVALID GROUP4 GROUP4 0 #{""} BCDEGHIX 090821 #{""} 16215-31588),
                %W(GROUP5 0 0 0 100117 #{""} 091217 #{""} 5527-43552),
                %W(EMAILIMP GROUP5 GROUP5 0 #{""} #{""} 090821 #{""} 1403-25394),
                %W(EXFILE GROUP5 GROUP5 0 #{""} #{""} 090821 #{""} 5439-86373),
                %W(IACOPY GROUP5 GROUP5 0 #{""} #{""} 090821 #{""} 4155-52246),
                %W(IAEXIDX GROUP5 GROUP5 0 #{""} #{""} 090821 #{""} 4178-29554),
                %W(IAEXIMG GROUP5 GROUP5 0 #{""} X 090821 #{""} 5432-51330),
                %W(IAEXPDF GROUP5 GROUP5 0 #{""} #{""} 090821 #{""} 14882-64178),
                %W(IAIPI GROUP5 GROUP5 0 #{""} BCDEMX 090821 #{""} 18143-05746),
                %W(IAMDW GROUP5 GROUP5 0 #{""} X 090821 #{""} 5522-76743),
                %W(IAPDEV GROUP5 GROUP5 0 #{""} #{""} 090821 #{""} 14835-27088),
                %W(IASTAMP GROUP5 GROUP5 0 #{""} #{""} 090821 #{""} 18894-26096),
                %W(IAXODBC2 GROUP5 GROUP5 0 #{""} X 090821 #{""} 6171-10407),
                %W(PAGEREG GROUP5 GROUP5 0 #{""} X 090821 #{""} 12786-30897),
                %W(SSOCR GROUP5 GROUP5 0 #{""} X 090821 #{""} 8841-85760),
                %W(VAL2XML GROUP5 GROUP5 0 #{""} #{""} 090821 #{""} 10118-17361),
                %W(IAEXDM 0 0 0 #{""} X 090821 #{""} 6099-31986),
                %W(SCANPLUS 2 0 0 #{""} CX 090821 #{""} 12763-69540),
                %W(WSINPUT 0 0 0 #{""} X 090821 #{""} 6821-93586),
                %W(WSOUTPUT 0 0 0 #{""} X 090821 #{""} 10827-16819),
                %W(PSGEMAIL 1 0 0 #{""} #{""} 090821 #{""} 2046-75860),
                %W(IAMKIMG 1 0 0 #{""} #{""} 090821 #{""} 8762-80338),
                %W(CLASSIF 1 100000 0 901231 #{""} 090821 #{""} 17522-49009/A),
                %W(DPCLSSF CLASSIF CLASSIF 0 #{""} BCDEGHIX 090821 #{""} 2142-93461),
                %W(DPMANAGR 6 0 0 #{""} BCDEGHIX 090821 #{""} 19562-72788),
                %W(EXTRACT 1 100000 0 901231 #{""} 090821 #{""} 2053-39552/A),
                %W(DPRECO EXTRACT EXTRACT 0 #{""} BCDEGHIX 090821 #{""} 6720-13285),
                %W(IASYSMON 1 0 0 #{""} #{""} 091217 #{""} 14898-20499)]
KAllComments = []