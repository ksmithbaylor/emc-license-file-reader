class EMCParser

  attr_accessor :modules, :sections

  def initialize(file=nil)
    @modules = []
    @file = file
    @sections = [:Module, :Copies, :Pages, :Valid, :EnterBy,
                 :Features, :Issued, :Disables, :Code]
  end

  def parse
    @file.each do |line|
      next if line[0] == "'" # Ignore comments
      @modules << line.split(";").map {|s| s.strip}
    end
  end

  # first ANNUAL, then GROUPS, space, everything else alpha

end